//
//  ViewController.swift
//  scroll_view
//
//  Created by NTAM on 1/16/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var featureScrollView: UIScrollView!
    var imageArray = [UIImage]()
    
    
    
//    let feature1=["title":"Apple Watch","price":"50.99","image":"1"]
//    let feature2=["title":"More Designs","price":"60.99","image":"2"]
//    let feature3=["title":"Notification","price":"70.99","image":"3"]
//    var featureArray=[Dictionary<String,String>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        featureScrollView.isPagingEnabled=true;
        featureScrollView.showsVerticalScrollIndicator=false
        
        featureScrollView.frame=view.frame
        imageArray = [#imageLiteral(resourceName: "ponti 1200X400-1fd1466cc3 - копия"),#imageLiteral(resourceName: "Carelia 1200X400"),#imageLiteral(resourceName: "Dollarphotoclub_60674903_opt-1200x400"),#imageLiteral(resourceName: "Lefkas-1200-x-400-slider-13"),#imageLiteral(resourceName: "tokyo-japan-1200x400")]
        for i in 0..<imageArray.count{
            let imageView = UIImageView()
            imageView.image = imageArray[i]
            imageView.contentMode = .scaleAspectFit
            let xposition = self.view.frame.width * CGFloat(i)
            imageView.frame=CGRect(x: xposition, y: 0, width: self.featureScrollView.frame.width, height: self.view.frame.height)
            featureScrollView.contentSize.width = featureScrollView.frame.width * CGFloat(i+1)
            featureScrollView.addSubview(imageView)
            print(imageArray[i])
        }
    }
    
}

